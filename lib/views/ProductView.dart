import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import '../models/ProductModel.dart';
import '../components/ProductWidget.dart';

class ProductView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ProductViewState();
  }
}

class ProductViewState extends State<ProductView> {
  List products;

  @override
  void initState() {
    super.initState();

    getProducts();
  }

  getProducts() async {
    var res = await productList.getProducts();

    setState(() {
     products = res; 
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("VStore", style: TextStyle(color: Colors.blue, fontFamily: "Pacifico")),
        backgroundColor: Colors.white,
        actions: <Widget>[
          FlatButton(
            child: Icon(
              Icons.update, 
              color: Colors.blue,
              size: 24,
            ),
            onPressed: () {
              getProducts();
            },
          )
        ],
      ),
      body: _getBody()
    );
  }

  _getBody() {
    if(products == null || products.length == 0) 
      return CircularProgressIndicator();
    else
      return ListView.builder(
        itemCount: products.length,
        itemBuilder: (context, i) {
          if(i < productList.length)
            return ProductWidget(product: productList[i]);
        },
      );
  }
}