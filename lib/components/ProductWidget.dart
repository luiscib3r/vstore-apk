import 'package:flutter/material.dart';

class ProductWidget extends StatelessWidget {
  const ProductWidget({
    @required this.product,
  });

  final Map<String, dynamic> product;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.network(product['imageURL']),
            Divider(),
            Padding(
              padding: EdgeInsets.all(6),
              child: Text(product['name'], style: TextStyle(fontSize: 24))
            ),
            Padding(
              padding: EdgeInsets.all(6),
              child: Text(product['description']),
            ),
            Divider(),
            Row(
              children: <Widget>[
                IconButton(
                  icon: Icon(Icons.star_border, color: Colors.yellowAccent),
                  onPressed: () => null,
                ),
                IconButton(
                  icon: Icon(Icons.star_border, color: Colors.yellowAccent),
                  onPressed: () => null,
                ),
                IconButton(
                  icon: Icon(Icons.star_border, color: Colors.yellowAccent),
                  onPressed: () => null,
                ),
                IconButton(
                  icon: Icon(Icons.star_border, color: Colors.yellowAccent),
                  onPressed: () => null,
                ),
                IconButton(
                  icon: Icon(Icons.star_border, color: Colors.yellowAccent),
                  onPressed: () => null,
                ),
              ],
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              textDirection: TextDirection.rtl,
            )
          ],
        ),
        margin: EdgeInsets.all(12),
    );
  }
}

